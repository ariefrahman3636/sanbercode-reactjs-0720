
// soal 1

 var kataPertama = "saya";
 var kataKedua = "senang";
 var kataKetiga = "belajar";
 var kataKeempat = "javascript";
 var kataKeempatUp = kataKeempat.toUpperCase();
 console.log(kataPertama + " " + kataKedua + " " + kataKetiga + " " + kataKeempatUp);

// soal 2

 var kataPertama = Number("1");
 var kataKedua = Number("2");
 var kataKetiga = Number("4");
 var kataKeempat = Number("5");
 console.log(kataPertama + kataKedua + kataKetiga + kataKeempat);

// soal 3

 var kalimat = 'wah javascript itu keren sekali'; 
 var kataPertama = kalimat.substring(0, 3); 
 var kataKedua = kalimat.substring(4, 14); 
 var kataKetiga = kalimat.substring(15, 18);  
 var kataKeempat = kalimat.substring(19, 24);
 var kataKelima = kalimat.substring(25, 31);  
 console.log('Kata Pertama: ' + kataPertama); 
 console.log('Kata Kedua: ' + kataKedua); 
 console.log('Kata Ketiga: ' + kataKetiga); 
 console.log('Kata Keempat: ' + kataKeempat); 
 console.log('Kata Kelima: ' + kataKelima);

// soal 4

var nilai = 15;

if(nilai >= 80) {
    console.log("indeksnya A");
} else if(nilai >= 70 && nilai < 80) {
    console.log("indeksnya B");
} else if(nilai >= 60 && nilai < 70) {
    console.log("indeksnya C");
} else if(nilai >= 50 && nilai < 60) {
    console.log("indeksnya D");
} else if(nilai < 50) {
    console.log("indeksnya E");
} else {
    console.log("nilai belum dimasukkan!")
}


// soal 5
 var hari = 22; 
 var bulan = 7; 
 var tahun = 2000;
 switch(bulan) {
     case 1: {console.log(hari + " January " + tahun);}
     break;
     case 2: {console.log(hari + " February " + tahun);}
     break;
     case 3: {console.log(hari + " March " + tahun);}
     break;
     case 4: {console.log(hari + " April " + tahun);}
     break;
     case 5: {console.log(hari + " May " + tahun);}
     break;
     case 6: {console.log(hari + " June " + tahun);}
     break;
     case 7: {console.log(hari + " July " + tahun);}
     break;
     case 8: {console.log(hari + " August " + tahun);}
     break;
     case 9: {console.log(hari + " September " + tahun);}
     break;
     case 10: {console.log(hari + " October " + tahun);}
     break;
     case 11: {console.log(hari + " November " + tahun);}
     break;
     case 12: {console.log(hari + " December " + tahun);}
     break;
 }