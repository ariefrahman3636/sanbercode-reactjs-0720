// soal 1

function halo() {
    console.log("Hallo Sanbers!");
}
console.log(halo());

// soal 2

function kalikan(num1, num2) {
    return num1 * num2;
}
var num1 = 6;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// soal 3

let name = "Steven";
let age = 18;
let address = "Jln. Malioboro, Yogyakarta";
let hobby = "Gaming";
function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age +
            " tahun, alamat saya di " + address + 
            ", dan saya punya hobby yaitu " + hobby + "!";
}
let perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
