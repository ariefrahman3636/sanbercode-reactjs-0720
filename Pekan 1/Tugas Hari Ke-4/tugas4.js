// Soal 1
// LOOPING PERTAMA
console.log("\n");
console.log("LOOPING PERTAMA");
let num = 2;
while (num <= 20) {
    console.log(num + " - I love coding");
    num += 2;
}
console.log("\n");
console.log("LOOPING KEDUA");

// LOOPING KEDUA
let num2 = 20;
while (num2 >= 2) {
    console.log(num2 + " - I will become a frontend developer");
    num2 -= 2;
}

// Soal 2
console.log("\n");
for (let i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 != 0) {
        console.log(i + " - I Love Coding");
    } else if (i % 2 != 0) {
        console.log(i + " - Santai");
    } else if (i % 2 == 0) {
        console.log(i + " - Berkualitas");
    }
}

// Soal 3
console.log("\n");
let empty = "";
for(i = 0; i < 7; i++) {
    for(j = 0; j <= i; j++) {
        empty += " #";
    }
    empty += "\n";
}
console.log(empty);

// Soal 4
console.log("\n");
var kalimat="saya sangat senang belajar javascript";
var kalimatArray = kalimat.split(" ");
console.log(kalimatArray);

// Soal 5
console.log("\n");
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var daftarBuahString = daftarBuah.sort().join("\n");
console.log(daftarBuahString);