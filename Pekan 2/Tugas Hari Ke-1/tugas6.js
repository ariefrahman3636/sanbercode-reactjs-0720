// // Soal 1
 var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
 var objectDaftarPeserta = {}
 objectDaftarPeserta.Nama = arrayDaftarPeserta[0]
 objectDaftarPeserta["Jenis Kelamin"]  = arrayDaftarPeserta[1]
 objectDaftarPeserta.Hobi = arrayDaftarPeserta[2]
 objectDaftarPeserta["Tahun Lahir"] = arrayDaftarPeserta[03]
 console.log(objectDaftarPeserta);

// // Soal 2

 var buah = [
  {
   nama: "strawberry",
   warna: "merah",
   "ada bijinya": "tidak",
   harga: 9000
  },
  {
   nama: "jeruk",
   warna: "oranye",
   "ada bijinya": "ada",
   harga: 8000
  },
  {
   nama: "Semangka",
   warna: "Hijau & Merah",
   "ada bijinya": "ada",
   harga: 10000
  },
  {
   nama: "Pisang",
   warna: "Kuning",
   "ada bijinya": "tidak",
   harga: 5000
  }
 ]
 console.log(buah[0]);

// Soal 3
var dataFilm = []

function tambahDataFilm(nama, durasi, genre, tahun) {
    var objectDataFilm = {}

    objectDataFilm.Nama = nama
    objectDataFilm.Durasi = durasi
    objectDataFilm.Genre = genre
    objectDataFilm.Tahun = tahun

    dataFilm.push(objectDataFilm);
    return objectDataFilm;
}
console.log(tambahDataFilm("Ini judul film", "Kalo ini durasi film", "Nah, yang ini genre film", 2020));

// Soal 4
// Release 0
class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
    get anam() {
        return this.name;
    }
}
var sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

// Release 1
class Ape extends Animal {
    constructor(name) {
        super(name);
    }
    yell() {
        return "Auooo";
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump() {
        return "hop hop";
    }
}
var sungokong = new Ape("kera sakti");
console.log(sungokong.yell());
var kodok = new Frog("buduk");
console.log(kodok.jump());

// Soal 5

class Clock {
    constructor({
        template
    }) {
        var timer;

        function render() {
            var date = new Date();

            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;

            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;

            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;

            var output = template
                .replace('h', hours)
                .replace('m', mins)
                .replace('s', secs);

            console.log(output);
        }

        this.stop = function () {
            clearInterval(timer);
        };

        this.start = function () {
            render();
            timer = setInterval(render, 1000);
        };
    }
}
var clock = new Clock({
    template: "h:m:s"
});
clock.start();