import React from 'react';
//import logo from './logo.svg';
import './App.css';


function App() {
  return (
    <div className="App">
      <h2>Form Pembelian Buah</h2>
      <div className="inputNama">
        <p className="nama">Nama Pelanggan</p>
        <input placeholder="nama"></input>
      </div>
      <p className="item">Daftar Item</p>
      <div className="fruits">
        <ul>
          <li><input type="checkbox" className="semangka"></input>Semangka</li>
          <li><input type="checkbox" className="jeruk"></input>Jeruk</li>
          <li><input type="checkbox" className="nanas"></input>Nanas</li>
          <li><input type="checkbox" className="anggur"></input>Anggur</li>
        </ul>
      </div>
      <button type="submit"><span>Kirim</span></button>
    </div>
  );
}

export default App;