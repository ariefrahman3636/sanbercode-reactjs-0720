// Soal 1
let keliling = (phi = 3.14, r) => 2 * phi * r;
console.log(keliling(3.14, 2));
let luas = (phi = 3.14, r) => phi * r * r;
console.log(luas(3.14, 4));

// Soal 2
let kalimat;
let saya = "saya"
let adalah = "adalah"
let seorang = "seorang"
let front = "frontend"
let dev = "developer"
 const tambahKata = (kalimat) => {
    kalimat =  `${saya}, ${adalah} ${seorang} ${front} ${dev}`;
    return kalimat;
  }

 console.log(tambahKata(kalimat))

 // Soal 3
 class Book {
     constructor(nama, totalPage, price) {
         this.nama = "Judul";
         this.totalPage = 999;
         this.price = 9999;
     }
     present() {
         return "I read a " + this.nama;
     }
 }
 class Komik extends Book {
     constructor(nama, totalPage, price, isColorfull) {
         super(nama, totalPage, price);
         this.isColorfull = true;
     }
     show() {
         return this.present() + ", it is a " + this.isColorfull;
     }
 }
 myBook = new Komik("One", 999, 9999)
console.log(myBook)