var readBooksPromise = require('./promise.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    },
    {
        name: 'Kalkulus2',
        timeSpent: 1000
    }
]


function bacaBuku(waktu, index) {
    console.log("Waktu yang saya miliki: " + waktu);
    readBooksPromise(waktu, books[index])
        .then(function (sisaWaktu) {
            if (sisaWaktu >=
                 0) {
                if (index + 1 < books.length) {
                    bacaBuku(sisaWaktu, index + 1)
                } else {
                    return;
                }
            }
        })
        .catch(function (waktuHabis) {
            return waktuHabis;
        })
}
bacaBuku(10000, 0)