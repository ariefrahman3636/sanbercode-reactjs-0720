// Soal 1
// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

function checkBaca(waktu, index) {
    readBooks(waktu, books[index], function(sisaWaktu) {
        if(sisaWaktu > 0) {
            if(index + 1 < books.length) {
                checkBaca(sisaWaktu, index + 1)
            } else {
                return;
            }
        }
    })
}
checkBaca(10000, 0)